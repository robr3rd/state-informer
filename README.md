# State Informer
Get info about states for LYle's school project.  Doing this by hand is tedious and consumes an astonishing amount of time.  ...which is ridiculous when I realized today, "Hang on, I'm a software engineer w/ a focus on web apps who is scraping web pages!  What am I doing with my life!?"

## Current notes
- Use [this](https://en.wikipedia.org/wiki/Georgia_(U.S._state)) as an example.  All states will work with the `_(U.S._state)` suffix even if the primary page/redirect doesn't.  So for simplicity we'll just do it for all and allow Wikipedia to "do the right thing" so that there's no ambiguity.
- Idea is to prompt for a state, grab that state's Wikipedia page, and then scrape off the following fields:
	- Capital
	- Abbreviation
	- Largest City
	- Year added to union
	- State bird (name + link to image)
	- State flag (link to image)
- Other fields on the worksheets that are not supported by this script--and are more fun to do together anyway--include the folliowing:
	- Unique thing you might see
	- Famous person
