#! /usr/bin/env python
# Modules for Jira interactions
import requests      # for API requests

# Modules for local script processing
from lxml import html                    # for parsing response's HTML

# Debugging tools
from pprint import pprint
#print(html.tostring(<Element goes here>))  # <- show element's HTML



# Functions
def request_page_content(page):
    protocol = 'https://'
    locale = 'en'
    base_url = 'wikipedia.org/wiki/'
    final_url = protocol + locale + '.' + base_url + page

    response = requests.get(final_url)
    return response.text


def get_detail(haytml, context, field_name):
    """
    'haytml' = 'haystack' + 'html' portmanteau.  I'm so sorry.
    """
    print('...finding "' + field_name + '" in "' + context + '"')

    if context == 'geography':
        table_filter = '[@class="infobox geography vcard"]'
    elif context == 'symbol':  # TODO haven't proven this to work yet (for the bird)
        table_filter = '[@class="infobox mw-collapsible uncollapsed mw-made-collapsible"]'

    primary_xpath = '//table' + table_filter + '/tbody/tr'
    primary_filter = '[th/a/text()="' + field_name + '"]'
    value_xpath = '/td'

    # Sometimes the capital is also the largest city.  When this happens, the fields are sort of combined.  This finds them.
    try:
        detail = haytml.xpath(primary_xpath + primary_filter + value_xpath)[0].text_content()
    except:
        field_name = 'and ' + field_name.lower()
        primary_filter = '[th/span/a/text()="' + field_name + '"]'
        detail = haytml.xpath(primary_xpath + primary_filter + value_xpath)[0].text_content()
    # if it fails again inside of the 'except', just let it fail...

    return detail


def process_content(content):
    output = {}
    root = html.fromstring(content)

    output.update(capital = get_detail(root, 'geography', 'Capital'))
    output.update(city = get_detail(root, 'geography', 'Largest city'))
    output.update(abbreviation = get_detail(root, 'geography', 'USPS abbreviation'))

    output.update(flag = 'https://en.wikipedia.org/wiki/' + state + '#/media/File:Flag_of_' + state + '.svg')

    # bird = ''

    return output


# ==============================================================================


# Handle input
state = input('Please a state name: ')
state = state + '_(U.S._state)'



# Execute
pagecontent = request_page_content(state)
pprint(process_content(pagecontent))